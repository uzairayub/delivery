package com.funutech.hunterduel.Utils;

public interface Constants {
    String BASE_URL = "https://mock-api-mobile.dev.lalamove.com/";
    String kStringNetworkConnectivityError = "Please make sure your device is connected with internet.";
    String EXCEPTION = "Exception";
    String EXCEPTION_MESSAGE = "Something went wrong while loading";
    String SERVER_EXCEPTION_MESSAGE = "Something went wrong, server not responding";


    String DELIVERIES_LIST = "deliveriesList";



}
