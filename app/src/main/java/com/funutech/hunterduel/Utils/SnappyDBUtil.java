package com.funutech.hunterduel.Utils;

import com.funutech.hunterduel.AppController;
import com.snappydb.DB;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.List;

public class SnappyDBUtil
{
    public static <T> boolean saveList(String key, List<T> list) {
        DB snappy = AppController.getSnappyInstance();
        try {
            if (snappy != null) {
                snappy.put(key, list);
            } else {
                snappy.del(key);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static <T> ArrayList<T> getList(String key, Class T) {
        DB snappy = AppController.getSnappyInstance();
        ArrayList<T> list = null;
        try {
            if (snappy != null) {
                list = snappy.getObject(key, ArrayList.class);
            }
        } catch (Exception e) {
            return list;
        }
        return list;
    }

    public static boolean deleteObjectOrList(String key) {
        DB snappy = AppController.getSnappyInstance();
        try {
            if (key != null) {
                snappy.del(key);
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
