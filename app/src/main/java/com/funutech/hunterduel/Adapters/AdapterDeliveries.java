package com.funutech.hunterduel.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.funutech.hunterduel.DeliveryDetailsCallBack;
import com.funutech.hunterduel.Models.DeliveriesResponse;
import com.funutech.hunterduel.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterDeliveries extends RecyclerView.Adapter<AdapterDeliveries.MyViewHolder> {

    private List<DeliveriesResponse> list;
    private Context context;
    private DeliveryDetailsCallBack callBack;

    public AdapterDeliveries(List<DeliveriesResponse> list, DeliveryDetailsCallBack callBack) {
        this.callBack = callBack;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_delivery, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        holder.deliveryDescription.setText(list.get(position).getDescription());
        String url = list.get(position).getImageUrl();
        if(url.contains("http"))
        {
            Picasso.get().load(url).placeholder(R.drawable.placeholder).into(holder.deliveryImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                callBack.onDeliveryClick(list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView deliveryImage;
        TextView deliveryDescription;

        public MyViewHolder(View itemView) {
            super(itemView);

            deliveryImage = itemView.findViewById(R.id.item_delivery_ImageView);
            deliveryDescription = itemView.findViewById(R.id.item_delivery_Description);
        }
    }
}
