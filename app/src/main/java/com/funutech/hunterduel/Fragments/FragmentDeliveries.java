package com.funutech.hunterduel.Fragments;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.funutech.hunterduel.Activities.MainActivity;
import com.funutech.hunterduel.Adapters.AdapterDeliveries;
import com.funutech.hunterduel.AppController;
import com.funutech.hunterduel.BroadcastReceiver.NetworkStateReceiver;
import com.funutech.hunterduel.DeliveryDetailsCallBack;
import com.funutech.hunterduel.Models.DeliveriesResponse;
import com.funutech.hunterduel.R;
import com.funutech.hunterduel.Utils.Constants;
import com.funutech.hunterduel.Utils.SnappyDBUtil;
import com.funutech.hunterduel.Utils.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDeliveries extends Fragment implements SwipeRefreshLayout.OnRefreshListener, DeliveryDetailsCallBack, NetworkStateReceiver.NetworkStateReceiverListener {

    private View view;
    private int offset = 0, limit = 15;
    private boolean noMoreItem,isLoading;
    private RecyclerView recyclerView;
    private List<DeliveriesResponse> list;
    private AdapterDeliveries adapter;
    private SwipeRefreshLayout refreshLayout;
    private NetworkStateReceiver networkStateReceiver;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_deliveries,container,false);
        initial();
        return view;
    }

    private void initial()
    {

        refreshLayout = view.findViewById(R.id.mainSwipeRefreshLayout);
        refreshLayout.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.mainRecyclerView);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1))
                {
                    if(!isLoading)
                        loadMore();
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list = new ArrayList<>();
        adapter = new AdapterDeliveries(list,this);
        recyclerView.setAdapter(adapter);
        refreshLayout.setRefreshing(true);
        loadDeliveries();
    }


    private void loadDeliveries()
    {
        if (!Util.isConnectingToInternet(getContext())) {
            Util.showToastMsg(getContext(), Constants.kStringNetworkConnectivityError);
            loadOffline();
            return;
        }
        isLoading = true;
        Call<List<DeliveriesResponse>> call = AppController.getInstance().getApiService().getDeliveriesListResponse(offset,limit);
        call.enqueue(new Callback<List<DeliveriesResponse>>() {
            @Override
            public void onResponse(Call<List<DeliveriesResponse>> call, Response<List<DeliveriesResponse>> response) {
                isLoading = false;
                refreshLayout.setRefreshing(false);
                try
                {

                    if(response.body().size() != 0)
                    {
                        list.addAll(response.body());
                    }
                    else
                    {
                        noMoreItem = true;
                    }
                    if(recyclerView.getVisibility() == View.GONE)
                        recyclerView.setVisibility(View.VISIBLE);
                    boolean issaed =  SnappyDBUtil.saveList(Constants.DELIVERIES_LIST, list);
                    adapter.notifyDataSetChanged();

                }
                catch (Exception e)
                {
                    Util.showToastMsg(getContext(), Constants.EXCEPTION_MESSAGE);
                    Log.e(Constants.EXCEPTION, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<List<DeliveriesResponse>> call, Throwable t)
            {
                isLoading = true;
                refreshLayout.setRefreshing(false);
                Log.e(Constants.EXCEPTION, t.getMessage());
                Util.showToastMsg(getContext(), Constants.SERVER_EXCEPTION_MESSAGE);
            }
        });
    }

    private void loadOffline()
    {
        list.clear();
        noMoreItem = true;
        ArrayList<DeliveriesResponse> x = SnappyDBUtil.getList(Constants.DELIVERIES_LIST,DeliveriesResponse.class);
        list.addAll(x);
        adapter.notifyDataSetChanged();
        refreshLayout.setRefreshing(false);

    }

    private void loadMore()
    {
        if(!noMoreItem)
        {
            offset = offset+15;
            loadDeliveries();
        }

    }

    @Override
    public void onRefresh()
    {
        refreshLayout.setRefreshing(true);
        list.clear();
        isLoading = false;
        noMoreItem = false;
        loadDeliveries();
    }

    @Override
    public void onDeliveryClick(DeliveriesResponse delivery)
    {
        MainActivity mainActivity = (MainActivity)getContext();
        mainActivity.openDeliveryDetails(delivery);
    }


    @Override
    public void onResume() {
        super.onResume();
        // Register the network state receiver to listen to network state change
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkStateReceiver();
            networkStateReceiver.addListener(this);
            getActivity().registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
            getActivity().registerReceiver(networkStateReceiver, new IntentFilter("location"));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (networkStateReceiver != null) {
            networkStateReceiver.removeListener(this);
            getActivity().unregisterReceiver(networkStateReceiver);
            networkStateReceiver = null;
        }
    }

    @Override
    public void networkAvailable()
    {
        noMoreItem = false;
        list.clear();
        loadDeliveries();
    }

    @Override
    public void networkUnavailable()
    {

    }
}
