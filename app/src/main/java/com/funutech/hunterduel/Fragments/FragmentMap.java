package com.funutech.hunterduel.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.funutech.hunterduel.Activities.MapsActivity;
import com.funutech.hunterduel.Models.DeliveriesResponse;
import com.funutech.hunterduel.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;


public class FragmentMap extends Fragment implements OnMapReadyCallback {


    private MapView mapView;
    private GoogleMap googleMap;
    private View view;
    private DeliveriesResponse delivery;
    private ImageView detailsImage;
    private TextView detailsDescription;

    public void setDelivery(DeliveriesResponse delivery) {
        this.delivery = delivery;
        updateMap();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map, container, false);
        initial(savedInstanceState);
        return view;
    }

    private void initial(Bundle savedInstanceState)
    {
        detailsImage = view.findViewById(R.id.detailsImage);
        detailsDescription = view.findViewById(R.id.detailsDescription);
        if(delivery != null)
        {
            Picasso.get().load(delivery.getImageUrl()).placeholder(R.drawable.placeholder).into(detailsImage);
            detailsDescription.setText(delivery.getDescription());
        }
        mapView = view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        updateMap();
        if (checkPermission()) {
            googleMap.setMyLocationEnabled(true);

        }

    }


    @SuppressLint("NewApi")
    private boolean checkPermission() {
        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            requestPermission();
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101 && grantResults[0] == PackageManager.PERMISSION_GRANTED && googleMap != null) {
            googleMap.setMyLocationEnabled(true);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }


    private void updateMap() {
        if (googleMap != null && delivery != null) {
            LatLng sydney = new LatLng(delivery.getLocation().getLat(), delivery.getLocation().getLng());
            googleMap.addMarker(new MarkerOptions().position(sydney).title(delivery.getLocation().getAddress()));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
        }
    }
}
