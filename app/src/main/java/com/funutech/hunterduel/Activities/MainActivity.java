package com.funutech.hunterduel.Activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.funutech.hunterduel.Fragments.FragmentDeliveries;
import com.funutech.hunterduel.Fragments.FragmentMap;
import com.funutech.hunterduel.Models.DeliveriesResponse;
import com.funutech.hunterduel.R;

public class MainActivity extends AppCompatActivity{

    private FragmentMap fragmentMap;
    private FragmentDeliveries fragmentDeliveries;
    private SelectedFrag selectedFrag;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initial();
    }

    @Override
    public void onBackPressed()
    {
        int count = fragmentManager.getBackStackEntryCount();
        if (count > 0) {
            if(count == 1)
            {
                finish();
            }
            else
            {
                fragmentManager.popBackStack();
            }

        }
        else {
            finish();
        }
    }

    private void initial()
    {
        fragmentManager = getSupportFragmentManager();
        Toolbar toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        replaceFragment(getFragmentDeliveries());
        selectedFrag = SelectedFrag.FragmentDelivery;
    }

    public void openDeliveryDetails(DeliveriesResponse delivery)
    {
        if(getFragmentMap() != null)
        {
            fragmentMap.setDelivery(delivery);
            replaceFragment(getFragmentMap());
            selectedFrag = SelectedFrag.FragmentMap;
        }
    }

    private FragmentMap getFragmentMap()
    {
        if(fragmentMap == null)
        {
            fragmentMap = new FragmentMap();
        }
        return fragmentMap;
    }

    private FragmentDeliveries getFragmentDeliveries()
    {
        if(fragmentDeliveries == null)
        {
            fragmentDeliveries = new FragmentDeliveries();
        }
        return fragmentDeliveries;
    }

    private void replaceFragment(Fragment fragment)
    {
        int count = fragmentManager.getBackStackEntryCount();
        if(count > 1)
        {
            fragmentManager.popBackStack();
        }
        else
        {
            fragmentManager.beginTransaction().replace(R.id.mainContainer,fragment).addToBackStack("").commit();
        }
    }

    private enum SelectedFrag{
        FragmentMap, FragmentDelivery;
    }
}
