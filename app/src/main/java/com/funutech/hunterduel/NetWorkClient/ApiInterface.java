package com.funutech.hunterduel.NetWorkClient;

import com.funutech.hunterduel.Models.DeliveriesResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("deliveries")
    Call<List<DeliveriesResponse>> getDeliveriesListResponse(@Query("offset") int offset,
                                                            @Query("limit") int limit);

}
