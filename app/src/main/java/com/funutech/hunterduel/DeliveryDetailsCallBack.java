package com.funutech.hunterduel;

import com.funutech.hunterduel.Models.DeliveriesResponse;

public interface DeliveryDetailsCallBack {

    public void onDeliveryClick(DeliveriesResponse delivery);
}
